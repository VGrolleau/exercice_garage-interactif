<?php
class Voiture extends Vehicule
{
    public function displayCharacteristics()
    {
        parent::displayCharacteristics();
        echo "<br/>Je suis la voiture " . $this->getMarque() . ' ' . $this->getModele();
    }
}
