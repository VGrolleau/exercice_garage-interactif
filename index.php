<?php
require "bootstrap.inc";

echo "<h1>Exercice sur les objets</h1> <hr/>";

$vehiculesTerrestres = [
    new Voiture("Renault", "Zoé", 4, 4),
    new Voiture("Citroën", "Berlingo", 4, 4),
    new Camion("Renault", "Premium", 10, 2, 26, 12),
    new Camion("Mercedes", "ZZ33", 10, 2, 26, 12)
];

$monGarage = new Garage($vehiculesTerrestres);
$monGarage->verifVehicules();
