<?php
class Garage
{
    private $vehicules;

    public function __construct(array $vehicules = [])
    {
        $this->addVehicules($vehicules);
    }

    public function setVehicule(Vehicule $vehicule)
    {
        $this->vehicule = $vehicule;
    }

    public function getVehicule()
    {
        return $this->vehicule;
    }
  
    public function addVehicules(array $vehicules) {
        foreach ($vehicules as $value){
            $this->vehicules[] = $value;
        }
    }
  
    public function addVehicule(Vehicule $vehicule) {
      	$this->vehicules[] = $vehicule;
    }
  
  	public function verifVehicules(){
      	foreach ($this->vehicules as $vehicule){
            if (random_int(1, 100) <= 25) {
                $vehicule->demarre()->avance(random_int(3, 20))->freine()->verifIsStopped();
            } else {
                $vehicule->demarre()->avance(random_int(3, 20))->freine()->arrete()->verifIsStopped();
            }
        }
    }
}