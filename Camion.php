<?php
class Camion extends Vehicule
{
    private $tonnage;
    private $remorqueLongueur;

    public function __construct(
        string $marque,
        string $modele,
        int $nbRoues,
        int $nbPortes,
        float $tonnage,
        int $remorqueLongueur
    ) {
        parent::__construct($marque, $modele, $nbRoues, $nbPortes);
        $this->setTonnage($tonnage);
        $this->setRemorqueLongueur($remorqueLongueur);
    }

    public function setTonnage(float $tonnage)
    {
        $this->tonnage = $tonnage;
    }

    public function getTonnage()
    {
        return $this->tonnage;
    }

    public function setRemorqueLongueur(int $remorqueLongueur)
    {
        $this->remorqueLongueur = $remorqueLongueur;
    }

    public function getRemorqueLongueur()
    {
        return $this->remorqueLongueur;
    }
}
