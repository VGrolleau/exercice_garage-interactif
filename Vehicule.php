<?php
class Vehicule
{

    private $nbRoues;
    private $nbPortes;
    private $marque;
    private $modele;
    private $isStopped;


    public function __construct(
        string $marque,
        string $modele,
        int $nbRoues,
        int $nbPortes

    ) {
        $this->setNbRoues($nbRoues);
        $this->setNbPortes($nbPortes);
        $this->setMarque($marque);
        $this->setModele($modele);
        $this->isStopped = true;
    }

    public function getNbRoues()
    {
        return $this->nbRoues;
    }

    public function getNbPortes()
    {
        return $this->nbPortes;
    }

    public function setNbRoues(int $nbRoues)
    {
        $this->nbRoues = $nbRoues;
    }

    public function setNbPortes(int $nbPortes)
    {
        $this->nbPortes = $nbPortes;
    }

    public function setMarque(string $marque)
    {

        $this->marque = $marque;
    }

    public function setModele(string $modele)
    {
        $this->modele = $modele;
    }

    public function getModele()
    {
        return $this->modele;
    }

    public function setIsStopped(bool $isStopped)
    {
        $this->isStopped = $isStopped;
    }

    public function getIsStopped()
    {
        return $this->isStopped;
    }

    public function getMarque()
    {
        return $this->marque;
    }

    public function demarre()
    {
        echo "<br/>Le véhicule " . $this->getMarque() . ' ' . $this->getModele() . " démarre.";
        return $this;
    }

    public function avance(int $metres)
    {
        echo "<br/>Il avance de $metres mètres.";
        $this->isStopped = false;
        return $this;
    }

    public function freine()
    {
        echo "<br/>Il freine.";
        return $this;
    }

    public function arrete()
    {
        echo "<br/>Il s'arrête.";
        $this->isStopped = true;
        return $this;
    }

    public function verifIsStopped()
    {
        if (true == $this->getIsStopped()) {
            echo "<br/>Félicitation, votre véhicule est certifié !<hr/>";
        } else {
            echo "<br/>Comme c'est dommage, il ne s'est pas arrêté... ESSAIES ENCORE !!<hr/>";
        }
    }
}
